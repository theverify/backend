const graph = require('fb-node');
const Validator = require('validatorjs');
const FindMyFacebookId = require('./../../utility/FindMyIdFacebook');
const {Accounts,Profile} = require('./../../models')


const addPageByToken = async function (req, res) {

    let rules = {
        token: 'required',
        url:'required|url'
      };
    let validation = new Validator(req.body, rules);

    if(! validation.passes()) {
      return res.status(200).json({
            success: false
        });
    }
    let fbuid = req.fbuid
    new FindMyFacebookId().getId(req.body.url, async function (id) {
        graph.token = req.body.token;
        await graph.get('/' + id).then(async function (dataPage) {
            let addPageTodb = await Profile.create({ profile_id: dataPage.json.id, name:dataPage.json.name,block:"123213",accounts_id:fbuid}).then( (res) => { return res} )
            res.status(200).json({
                success: true,
                data: dataPage.json
            });
        }).catch(async function (e) {
            res.status(200).json({
                success: false
            });
        })
    })

}

module.exports = addPageByToken