const graph = require('fb-node');
const FindMyFacebookId = require('./../../utility/FindMyIdFacebook');
const Validator = require('validatorjs');


const getPageByToken = async function (req, res) {

    let rules = {
        token: 'required',
        url:'required|url'
      };
    let validation = new Validator(req.body, rules);

    if(! validation.passes()) {
      return res.status(200).json({
            success: false
        });
    }
    let token = req.body.token
    let url = req.body.url

    new FindMyFacebookId().getId(url, async function (id) {
        graph.token = token;
        await graph.get('/' + id).then(async function (data) {
          res.status(200).json({
                success: true,
                data: data.json
            });
        }).catch(async function (e) {
            res.status(200).json({
                success: false
            });
        })
    })

}


module.exports = getPageByToken;
