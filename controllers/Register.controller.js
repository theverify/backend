const graph = require('fb-node');
const Validator = require('validatorjs');
const {Accounts} = require('../models')
const {createWalllet} = require('./../services/Blockchain/Wallet')
async function getInfomation(access_token) {
        graph.token = access_token;
        return await graph.get('/me').then(async function (data) {
            return await data.json;
        }).catch(async function (e) {
            return await e;
        })
}

const Register = async function (req, res) {

    let rules = {
        token: 'required',
        nationalId:'required',
        firstname:'required',
        lastname:'required',
        birthday:'required'
      };
    let validation = new Validator(req.body, rules);
    if(! validation.passes()) {
      return res.status(200).json({
            success: false
        });
    }
    let wallet = await createWalllet();
    console.log(wallet)
    let data  = await getInfomation(req.body.token)
    Accounts.create({
        fbuid:data.id,
        national_id: req.body.nationalId,
        firstname: req.body.firstname,
        lastname:req.body.lastname,
        birthday:req.body.birthday,
        private_key:wallet.privateKey,
        wallet_address:wallet.address
    }).then(function (succcess) {
        res.status(200).json({
            success: true,
            data: succcess
        });
    })
}

module.exports = Register;