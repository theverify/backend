const graph = require('fb-node');
const Validator = require('validatorjs');
const {Accounts} = require('./../models')

async function getInfomation(access_token) {
        graph.token = access_token;
        return await graph.get('/me').then(async function (data) {
            return await data.json;
        }).catch(async function (e) {
            return await e;
        })
}

const PreLogin = async function (req, res) {

    let rules = {
        token: 'required'
      };
    let validation = new Validator(req.body, rules);
    if(! validation.passes()) {
      return res.status(401).json({
            success: false
        });
    }
    let data  = await getInfomation(req.body.token)
    console.log(data)
    Accounts.findOne({ where: { fbuid: data.id  } }).then(async function (resSQL) {
        if (resSQL == null) {
            return  res.status(200).json({
                success: false,
                message: "USER_NOT_FOUND"
            });
            
        }
      return  res.status(200).json({
            success: true
        });
    })

}

module.exports = PreLogin;