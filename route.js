const express = require('express')
const router = express.Router();
const getInfoPage = require('./controllers/User/Pages.controller')
const addPageByToken = require('./controllers/User/AddPage.controller')
const PreLogin = require('./controllers/PreLogin.controller')
const Register = require('./controllers/Register.controller')

const UserMiddleware = require('./middleware/UserMiddleware')


router.post('/prelogin',PreLogin)

router.post('/register',Register)

router.use('/user',UserMiddleware)

router.post('/user/getinfopage',getInfoPage)
router.post('/user/addpage',addPageByToken)


module.exports = router;
