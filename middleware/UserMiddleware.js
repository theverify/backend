const cookie = require('cookie');
const graph = require('fb-node');
const Validator = require('validatorjs');
const {Accounts} = require('./../models')

async function getInfomation(access_token) {
        graph.token = access_token;
        return await graph.get('/me').then(async function (data) {
            return await data.json;
        }).catch(async function (e) {
            return await e;
        })
}

const UserMiddleware = async function (req, res, next) {
    let rules = {
        token: 'required'
      };
    let validation = new Validator(req.body, rules);
    if(! validation.passes()) {
      return res.status(401).json({
            success: false
        });
    }
    let data  = await getInfomation(req.body.token)
    Accounts.findOne({ where: { fbuid: data.id  } }).then(async function (resSQL) {
        if (resSQL == null) {
            return  res.status(200).json({
                success: false,
                message: "USER_NOT_FOUND"
            });
            
        }
        await res.setHeader('Set-Cookie', cookie.serialize('fbuid', String(data.id), {
            httpOnly: false,
            maxAge: 60 * 60 * 24 * 7 // 1 week
          }))
          req.fbuid = data.id
          req.token = req.body.token
          next()
    })
    
}

module.exports = UserMiddleware;