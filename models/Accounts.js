const { Sequelize,sequelize } = require("./../services/Sequelize")

const Model = Sequelize.Model;
class Accounts extends Model {}
Accounts.init({
  national_id: {
    type: Sequelize.STRING(64),
    allowNull: false
  },wallet_address: {
    type: Sequelize.STRING(64),
    allowNull: false
  },private_key: {
    type: Sequelize.STRING(512),
    allowNull: false
  }, firstname: {
    type: Sequelize.STRING(64),
    allowNull: false
  },lastname: {
    type: Sequelize.STRING(64),
    allowNull: false
  },fbuid: {
    type: Sequelize.STRING(128),
    allowNull: false
  },birthday: {
    type: Sequelize.DATE,
    allowNull: false
  },kyc: {
    type: Sequelize.TINYINT
  }
}, {
  sequelize,
  modelName: 'account'
  // options
});

module.exports = Accounts;