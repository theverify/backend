const { Sequelize,sequelize } = require("../services/Sequelize")

const Model = Sequelize.Model;
class Profiles extends Model {}
Profiles.init({
  // attributes
  profile_id: {
    type: Sequelize.STRING(64),
    allowNull: false
  },
  name: {
    type: Sequelize.STRING(64),
    allowNull: false
  },
  detail:{
    type:Sequelize.TEXT
  },
  block:{
    type:Sequelize.STRING(64)
  },
  accounts_id:{
    type:Sequelize.INTEGER
  }
}, {
  sequelize,
  modelName: 'profile'
  // options
});

module.exports = Profiles;