const Accounts = require("./Accounts")
const Profile = require("./Profiles")


Accounts.hasMany(Profile); 
Profile.belongsTo(Accounts);

module.exports = {Accounts,Profile};