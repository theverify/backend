const Web3 = require('web3');
const {RpcUrl} = require('./Config');
let web3 = new Web3();
web3.setProvider(new web3.providers.HttpProvider(RpcUrl));

const createWalllet =  async function (){
    return  await web3.eth.accounts.create()
}

module.exports = {createWalllet};