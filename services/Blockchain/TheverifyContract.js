const Web3 = require('web3');
const {RpcUrl,ContractTheverify } = require('./Config');

/* Connect to ethereum node */
const abi =[
	{
		"constant": false,
		"inputs": [
			{
				"internalType": "string",
				"name": "id",
				"type": "string"
			}
		],
		"name": "register_profile",
		"outputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"internalType": "address",
				"name": "addr",
				"type": "address"
			}
		],
		"name": "register_voter",
		"outputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"inputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "constructor"
	},
	{
		"constant": false,
		"inputs": [
			{
				"internalType": "string",
				"name": "id",
				"type": "string"
			}
		],
		"name": "vote",
		"outputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [
			{
				"internalType": "uint256",
				"name": "i",
				"type": "uint256"
			}
		],
		"name": "get_candidate",
		"outputs": [
			{
				"internalType": "string",
				"name": "_candidate",
				"type": "string"
			},
			{
				"internalType": "uint256",
				"name": "_votes",
				"type": "uint256"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [],
		"name": "get_num_candidates",
		"outputs": [
			{
				"internalType": "uint256",
				"name": "",
				"type": "uint256"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	}
]

let web3 = new Web3();
web3.setProvider(new web3.providers.HttpProvider(RpcUrl));


/*Call the function which already deployed on ethereum network
  Notice: ABI have to modifeid when the smart contract code change*/
var TheverifyContract =  new web3.eth.Contract(abi,ContractTheverify);

module.exports  = TheverifyContract;